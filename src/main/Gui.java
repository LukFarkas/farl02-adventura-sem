package main;

import GrafickeZpracovani.PanelBatohu;
import GrafickeZpracovani.PanelVychodu;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import logika.Hra;
import logika.IHra;
import GrafickeZpracovani.HerniPlocha;
import uiText.TextoveRozhrani;

/**
 * Třída Gui spouští grafické rozhraní
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public class Gui extends Application {


    private Button button;
    private String clickMetext;
    private Label zadejPrikazLabel;
    private FlowPane spodniFlowPane;
    private TextField prikazovePole;
    private IHra hra= new Hra();
    private TextArea herniKonzole;
    private  final HerniPlocha herniPlocha= new HerniPlocha(hra.getHerniPlan());
    private popiskyZmacknuti aktualniPopisTlacitka  = popiskyZmacknuti.NULAKrat;
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;

    /**
     * metoda ktera spousti aplikaci a vybira mezi grafickym a textovym rozhranim
     * @param args
     */
    public static void main(String[] args) {
       if(args.length==0){
        launch(args);
       }else if (args[0].equals("-text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
            System.exit(0);
        }
    }

    /**
     * hlavni metada ktera vola vsechny metody inicializuje prvky pro vytvoreni grafickeho rozhrani
     *
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
       // vytvorTlacitkoProUcitele();
        vytvorSpodniPanel();


        herniKonzole = new TextArea();
        herniKonzole.setText(hra.vratUvitani());
        herniKonzole.appendText("\n");
        herniKonzole.setEditable(false);


        VBox hlavniBox = new VBox();


        BorderPane pane = new BorderPane();
        pane.setCenter(herniKonzole);

        //menu
        MenuBar menuBar = vytvorMenu();

        //toolbar
        ToolBar toolBar = toolbar();


        hlavniBox.getChildren().addAll(menuBar,toolBar,pane);

        panelBatohu = new PanelBatohu(hra.getHerniPlan().getBatoh());
        pane.setLeft(panelBatohu.getPanel());


        pane.setBottom(spodniFlowPane);
        pane.setTop(herniPlocha.getAnchorPane());
        panelVychodu = new PanelVychodu(hra.getHerniPlan());
        pane.setRight(panelVychodu.getListWiew());

        pripravPrikazovePole(herniKonzole);

        //nastavi Layout panel
        Scene scene = new Scene(hlavniBox, 900,800);
        // vytvari hlavni okno
        primaryStage.setScene(scene);
        primaryStage.setTitle("the click me app");
        prikazovePole.requestFocus();
        primaryStage.show();

    }

    /**
     * Metoda vytvari toolbar a tlacitko prozkoumej
     *
     * @return
     */
    private ToolBar toolbar() {
        Button prozkoumej = new Button("prozkoumej");
        ToolBar toolBar= new ToolBar(prozkoumej);
        prozkoumej.setOnAction(event -> {
            String odpovedHry=hra.zpracujPrikaz("prozkoumej");
            herniKonzole.appendText("\n"+ odpovedHry+ "\n");
        });
        return toolBar;
    }


    /**
     * metoda vytvari menubar a prirazuje do nej polozky
     *
     * @return
     */
    private MenuBar vytvorMenu() {

        Menu menuSoubor = new Menu("Soubor");
        Menu menuNapoveda =new Menu("Napoveda");
        MenuBar menuBar = new MenuBar(menuSoubor,menuNapoveda);

        //ImageView novaHraIkonka = new ImageView(new Image(Gui.class.getResourceAsStream("/Zdroje/new.gif")));

        MenuItem novaHraMenuItem = novaHraMenuItem();


        MenuItem konecMenuItem = new MenuItem("Konec");

        konecMenuItem.setOnAction(event -> System.exit(0));
        menuSoubor.getItems().addAll(novaHraMenuItem, new SeparatorMenuItem(),konecMenuItem);

        MenuItem oAplikaciMenuItem= new MenuItem("O aplikaci");
        MenuItem napovedaMenuItem=new MenuItem("Napoveda");
        menuNapoveda.getItems().addAll(oAplikaciMenuItem,new SeparatorMenuItem(),napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event->{
           Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
           alert.setTitle("graficka Adventura");
           alert.setHeaderText("javasFX adventura");
           alert.setContentText("verze ZS 2020");
           alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Nápověda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(Gui.class.getResource("/Zdroje/info.html").toExternalForm());
            stage.setScene(new Scene(webView,500,500));
            stage.show();


        });

        return menuBar;
    }

    /**
     * Vytvari polozku nova hra v menu
     *
     * @return
     */
    private MenuItem novaHraMenuItem() {
        MenuItem novaHraMenuItem = new MenuItem("Nova hra");
        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        novaHraMenuItem.setOnAction(t ->{
            hra =new Hra();
            herniKonzole.setText(hra.vratUvitani());
           hra.getHerniPlan().zaregistrujPozorovatele(herniPlocha);
            hra.getHerniPlan().zaregistrujPozorovatele(panelVychodu);
            hra.getHerniPlan().getBatoh().zaregistrujPozorovatele(panelBatohu);
            herniPlocha.novaHra(hra);
            panelVychodu.novaHra(hra);
            panelBatohu.novaHra(hra);
            prikazovePole.requestFocus();
        });
        return novaHraMenuItem;
    }

    /**
     * nacita informace z prikazoveho pole a odpovedi vypysuje na obrazovku
     *
     * @param herniKonzole
     */
    private void pripravPrikazovePole(TextArea herniKonzole) {
        prikazovePole.setOnAction(event -> {
            String zadanyPrikaz = prikazovePole.getText();
            herniKonzole.appendText("\n"+ zadanyPrikaz+ "\n");
            prikazovePole.setText(" ");

            String odpovedHry = hra.zpracujPrikaz(zadanyPrikaz);
            herniKonzole.appendText("\n"+ odpovedHry+ "\n");
            if (hra.konecHry()){
                prikazovePole.setEditable(false);
            }
        });
    }


    /**
     * vytvari a vypisuje spodni panel okna
     */
    private void vytvorSpodniPanel() {
        spodniFlowPane =new FlowPane();
        zadejPrikazLabel =new Label("Zadej prikaz");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD,16));
        prikazovePole = new TextField();
        spodniFlowPane.setAlignment(Pos.CENTER);
        spodniFlowPane.getChildren().addAll(zadejPrikazLabel, prikazovePole);
    }

    /**
     * vytvari tlacitko pro stavovy automat
     */
    private void vytvorTlacitkoProUcitele() {
        button = new Button();
        clickMetext ="zmacknime";
        button.setText(clickMetext);
        button.setOnAction(event -> zmacknoutTlacitko());
    }

    /**
     * text stavoveho automatu
     */
    public enum popiskyZmacknuti{
        NULAKrat("Zmackni me"), JEDNOU("Zmackli me"), DVAKRAT("mackas me"), trikrat("dost");

        private final String popisek;

         popiskyZmacknuti(String popisek) {
             this.popisek = popisek;
         }
     }

    /**
     *meni stavovy automat
     */
    private void zmacknoutTlacitko() {


        switch (aktualniPopisTlacitka){
            case NULAKrat:
                button.setText(popiskyZmacknuti.JEDNOU.popisek);
                aktualniPopisTlacitka = popiskyZmacknuti.JEDNOU;
                break;
            case JEDNOU:
                button.setText(popiskyZmacknuti.DVAKRAT.popisek);
                aktualniPopisTlacitka = popiskyZmacknuti.DVAKRAT;
                break;
            case DVAKRAT:
                button.setText(popiskyZmacknuti.trikrat.popisek);
                aktualniPopisTlacitka = popiskyZmacknuti.trikrat;
                break;
            case trikrat:
                button.setText(popiskyZmacknuti.NULAKrat.popisek);
                aktualniPopisTlacitka = popiskyZmacknuti.NULAKrat;
                break;

        }
       /* if(button.getText().equals(clickMetext)){
            button.setText("Zmackli me");
        } else {
            button.setText(clickMetext);
        }*/

    }
}
