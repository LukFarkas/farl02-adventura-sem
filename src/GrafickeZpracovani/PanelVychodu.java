package GrafickeZpracovani;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import logika.HerniPlan;
import logika.IHra;
import logika.Prostor;
import util.ObserverZmeny;
/**
 *Trida PanelVychodu nastavuje ListView a prirazuje do nej mozne vychody
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public class PanelVychodu implements ObserverZmeny {
    HerniPlan herniPlan;
    ListView<String> listWiew= new ListView<>();
    ObservableList<String> vychody = FXCollections.observableArrayList();

    /**
     * konstruktor PanelVychodu inicializuje herni plan a nastavuje listView
     * @param herniPlan
     */
    public PanelVychodu(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
        init();
        herniPlan.zaregistrujPozorovatele(this);
    }

    /**
     * nastavuje rozmery listView a aktualni vychody
     */
    private void init() {
        listWiew.setItems(vychody);
        listWiew.setPrefWidth(100);

         nactiAktualniVychody();
    }

    /**
     * pridava nazvy vychodu do ObservableList
     */
    private void nactiAktualniVychody() {
        for (Prostor prostor : herniPlan.getAktualniProstor().getVychody()) {
            vychody.add(prostor.getNazev());
        }
    }

    /**
     * vraci odkaz na listView
     * @return
     */
    public  ListView<String> getListWiew(){
        return listWiew;
    }

    /**
     *  smaze a znovu nacte aktualni vychody pomoci metody nactiAktualniVychody
     */
    @Override
    public void aktualizuj() {
        vychody.clear();
        nactiAktualniVychody();

    }

    /**
     * nacita herni plan pro spusteni nove hry
     * @param hra
     */
    public void novaHra(IHra hra) {
        this.herniPlan=hra.getHerniPlan();
        aktualizuj();
    }
}
