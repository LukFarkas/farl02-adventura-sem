package GrafickeZpracovani;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import logika.HerniPlan;
import logika.IHra;
import util.ObserverZmeny;
/**
 * Třída HerniPlocha představuje mapu a nastavuje pozici hrace
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public class HerniPlocha implements ObserverZmeny {
    private AnchorPane anchorPane = new AnchorPane();
    private Circle aktualniPozice;
    private  HerniPlan herniPlan;

    /**
     * konstruktor tridy HerniPlocha inicializuje herni plan, nacita mapu a pozici hrace
     *
     */
    public HerniPlocha(HerniPlan herniPlan){
        this.herniPlan = herniPlan;
        init();
        herniPlan.zaregistrujPozorovatele(this);
    }

    /**
     * nacita  mapu ze souboru Zdroje a pomoci metody nastavuje pozici hrace
     * data  predava do anchorPane
     */
    private void init() {
       // ImageView herniPlochaImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream("/Zdroje/Mapa.png"),900,500,false,false));
        ImageView herniPlochaImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream("/Zdroje/map2.jpg"),900,500,false,false));
        aktualniPozice = new Circle(10, Paint.valueOf("red"));
        //herniPlan.setAktualniProstor(herniPlan.getAktualniProstor().vratSousedniProstor("vykopávky"));

        nastavPoziciHrace();

        anchorPane.getChildren().addAll(herniPlochaImageView, aktualniPozice);
    }

    /**
     * metoda nacita souradnice z hernihoPlanu a nastavi pozici hrace pomoci
     */
    private void nastavPoziciHrace() {
        AnchorPane.setTopAnchor(aktualniPozice,herniPlan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(aktualniPozice,herniPlan.getAktualniProstor().getPosLeft());
    }

    /**
     * vraci odkaz pro anchorPane
     *
     * @return anchorPane
     */
    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    /**
     * pomocí metody nastavPoziciHrace nacte souradnice z hernihoPlanu a nastavi pozici hrace
     */
    @Override
    public void aktualizuj() {
        nastavPoziciHrace();

    }

    /**
     * vraci odkaz na herni plan pro novou hru
     * @param hra
     */
    public void novaHra(IHra hra) {
        this.herniPlan=hra.getHerniPlan();
        aktualizuj();
    }
}
