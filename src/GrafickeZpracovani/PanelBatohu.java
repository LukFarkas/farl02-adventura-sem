package GrafickeZpracovani;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import logika.Batoh;
import logika.IHra;
import util.ObserverZmeny;

import java.io.InputStream;
import java.util.Set;

/**
 * Třída panelBatohu nacita nazvy veci z batohu, propojuje je s obrazky ze souboru Zdroje
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public class PanelBatohu implements ObserverZmeny {
    private VBox vBox = new VBox();
    private FlowPane panelVeci = new FlowPane();
    private Batoh batoh;

    /**
     * konstruktor PanelBatohu vytváří a inicializuje batoh a VBox
     */
    public PanelBatohu(Batoh batoho){
        this.batoh=batoho;
        init();
        batoho.zaregistrujPozorovatele(this);

    }

    /**
     * nastavuje VBox
     */
    private void init() {
        vBox.setPrefWidth(100);
        Label label = new Label("Veci v batohu");
        vBox.getChildren().addAll(label,panelVeci);
        nactObrazkyVeci();

    }

    /**
     * nacita informace z batohu, informace ze zdroju a propojuje je
     */
    private void nactObrazkyVeci() {
        panelVeci.getChildren().clear();
        String nazevObrazku;
        Set<String> mnozinaVeci=batoh.getNazvyVeci();

        for (String vec : mnozinaVeci) {
            nazevObrazku = "/Zdroje/" +vec+".png";
            InputStream inputStream  =PanelBatohu.class.getResourceAsStream(nazevObrazku);
            //InputStream inputStream  =new Image(new FileInputStream(nazevObrazku));
            Image image = new Image(inputStream,100,100,false,false);

            /*Image image = null;
            try {
                image = new Image(new FileInputStream(nazevObrazku),100,100,false,false);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }*/
            ImageView vecImageView = new ImageView(image);
            panelVeci.getChildren().add(vecImageView);
        }
    }

    /**
     *pomocí metody nactObrazkyVeci nacita informace z batohu, informace ze zdroju a propojuje je
     */
    @Override
    public void aktualizuj() {
        nactObrazkyVeci();

    }

    /**
     * vraci odkaz na vbox
     *
     */
    public Node getPanel() {
        return this.vBox;
    }

    /**
     * nacita batoh pro spusteni nove hry
     * @param hra
     */
    public void novaHra(IHra hra) {
        this.batoh=hra.getHerniPlan().getBatoh();
        aktualizuj();
    }
}
