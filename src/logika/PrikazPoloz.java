package logika;
/**
 * třída PrikazPoloz implementuje pro hru pokládání věcí 
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazPoloz implements IPrikaz{
	
	 private static final String NAZEV = "poloz";
	    private HerniPlan plan;
	    /**
	     * konstruktor třidy
	     * @param plan ve kterem se budou pokládat věci
	     */
	    public PrikazPoloz(HerniPlan plan){
	    	this.plan=plan;
	    }

	    /**
	     *  Provádí příkaz "poloz".Položí věc z batohu do prostoru kde se hráč nachází.
	     *  pokud hráč nemá věc v batohu, vypíše se chybové hlášení.
	     *
	     *@param parametry - Věc kterou chce hráč položit
	     *@return zpráva, kterou vypíše hra hráči
	     */ 
	@Override
	public String provedPrikaz(String... parametry) {
		// TODO Auto-generated method stub
		 if (parametry.length == 0) {
	            
	            return "Co mám položit? musíš mi to říct...";
	        }
		 if(plan.getBatoh().ObsahujeVec(parametry[0])){
			 
		 Vec polozenaVec = plan.getBatoh().odeberVecZBatohu(parametry[0]);
	     plan.getAktualniProstor().vlozVec(polozenaVec);
	            
	            return "tohle jsi zde nechal: "+ polozenaVec.getJmeno();
	        }
		 else {return "Tohle v batohu nemáš";}
		 
	}
	
	 /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
