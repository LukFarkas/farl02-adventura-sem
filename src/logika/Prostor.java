package logika;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 * 
 * Prostor eviduje postavy a věci které se v něm nacházejí.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 * @version pro školní rok 2016/2017
 */
public class Prostor {

    private final double posLeft;
    private final double posTop;
    private String nazev;
    private String popis;
    private Set<Prostor> vychody;   // obsahuje sousední místnost
    private Map<String, IPostava> postavy = new HashMap<>();
    private Map<String, Vec> seznamVeci = new HashMap<>();
    //zamykání místnosti
    private boolean zamcena = false;
    
    /**
     * nastaví jestli je prostor zamknutý
     * @paramtrue pokud ano, false pokud ne
     */
    public void zamknout(boolean zamcena){
    	
    	this.zamcena= zamcena;
    }
    
    /**
     * zjistí jestli je prostor zamknutý
     * @paramtrue pokud ano, false pokud ne
     */
    public boolean jeZamcena(){
    	return zamcena;
    }
    
    //věc----------
    
    
    /**
     * vloží věc do seznamu
     * @paramvěc kterou chceme vložit
     * @return věc kterou jsme vložili
     */
    public Vec vlozVec(Vec vec) {
        seznamVeci.put(vec.getJmeno(),vec);
        if (seznamVeci.containsKey(vec.getJmeno())) {
            return vec;
        }
        return null;
    }
    
    /**
     * vrátí seznam věcí
	 * @return mapa s věcmi
	 */
	public Map<String, Vec> getSeznamVeci() {
		return seznamVeci;
	}

	/**
	 * nastaví seznam věcí
	 * @parammapa s věcmi
	 */
	public void setSeznamVeci(Map<String, Vec> seznamVeci) {
		this.seznamVeci = seznamVeci;
	}

	/**
	 * odebere věc ze seznamu
	 * @param vec kterou chceme odebrat
	 * @return věc kterou jsme odebrali
	 */
	public Vec odeberVec(String vec){
    	return seznamVeci.remove(vec);
    }
    /**
     * vrátí věci které jsou v prostoru
     * @return String s věcmi v prostoru a popisem
     */
    private String popisVeci() {
        String vracenyText = "Věci k sebrání: ";
        if(seznamVeci.size()>0) {
            for (Map.Entry<String, Vec> vec : seznamVeci.entrySet()) {
            	if(vec.getValue().jeProzkoumana()){
                vracenyText += vec.getValue().getJmeno() + ",";
            	}
            }
            return vracenyText;
        }
        else {
            return vracenyText += "Nic tu není";
        }
    }
    
    /**
     * zjistí jestli prostor obsahuje určitou věc
     * @param jmeno věci
     * @return true pokud je věc v prostoru false pokud ne.
     */
    public boolean obsahujeVec(String jmeno){
    	return seznamVeci.containsKey(jmeno);
    }
    
    /**
     * vrátí věc která je v prostoru
     * @paramString, jmeno věci
     * @return Věc kterou voláme
     */
    public Vec getVecZProstoru(String jmeno){
    	return seznamVeci.get(jmeno);
    }
    
    
    
    //postavy
    /**
     * vlozi postavu
     * @param postava kterou chceme vlozit
     * @return postava vrati postavu kterou jsem vlozili
     */
    public IPostava vlozPostavu(IPostava postava) {
    	//podle testu změnit na Void
        postavy.put(postava.getJmeno(), postava);
       // if (veci.containsKey(postava.getJmeno())) {
            return postava;
    }
    
    /**
     * odstraní postavu
     * @param jmeno postavy kterou chceme odstranit
     * @return postavu kterou jsme odstranili
     */
    public IPostava odstranPostavu(String jmeno){
    	return postavy.remove(jmeno);
    }
    /**
     * zjistí jestli je postava v prostoru
     * @param jmeno postavy
     * @return vratí true pokud postava je v prostoru
     */
    public boolean obsahujePostavu(String jmeno){
    	return postavy.containsKey(jmeno);
    }
    /**
     * vrátí postavu kterou voláme podle jména
     * @param nazev postavy
     * @return IPostava kterou voláme
     */
    public IPostava vratPostavu(String nazev) {
        return postavy.get(nazev);
    }
    
    /**
     * vypíše všechny postavy které jsou v prostoru
     * @return String s postavami a textem 
     */
    private String popisPostavy() {
        String vracenyText = "postavy: ";
        if(postavy.size()>0) {
            for (Map.Entry<String, IPostava> postava : postavy.entrySet()) {
                vracenyText += postava.getValue().getJmeno()+ ", ";
            }
            return vracenyText;
        }
        else {
            return vracenyText += "zde nejsou žádné postavy";
        }
    }
    
    
    /**
     * Vytvoření prostoru se zadaným popisem, např. "kuchyň", "hala", "trávník
     * před domem"
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     * víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis, double posLeft, double posTop) {
        this.posLeft = posLeft;
        this.posTop= posTop;
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
       // seznamVeci = new ArrayList<Vec>();
    }
    
    
    

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     *
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */  
      @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

       return (java.util.Objects.equals(this.nazev, druhy.nazev));       
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }
      

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;       
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v mistnosti/prostoru " + popis + ".\n"
                + popisVychodu()
                + ".\n "
                + popisVeci()+
                "\n "
                + popisPostavy();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    private String popisVychodu() {
        String vracenyText = "východy:";
        for (Prostor sousedni : vychody) {
            vracenyText += " " + sousedni.getNazev();
            if (sousedni.jeZamcena()) {
                vracenyText += "(zamknuto)";
            }
        }
        return vracenyText;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor>hledaneProstory = 
            vychody.stream()
                   .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                   .collect(Collectors.toList());
        if(hledaneProstory.isEmpty()){
            return null;
        }
        else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * vraci souradnice prostoru pro lokalizaci hrace
     * @return souradnice leve strany
     */
    public double getPosLeft() {
        return posLeft;
    }

    /**
     * vraci souradnice prostoru pro lokalizaci hrace
     * @return souradnice horni strany
     */
    public double getPosTop() {
        return posTop;
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }
}
