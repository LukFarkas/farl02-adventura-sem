package logika;
/**
 * třída prikazOdemknout odemyká zavřené prostory
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazOdemknout implements IPrikaz{
	
	private static final String NAZEV = "odemkni";
    private HerniPlan plan;
    
    public PrikazOdemknout(HerniPlan plan){
    	this.plan = plan;
    	
    }
    
/**
 * zkontroluje jestli jsou splněny podmínky pro otevření prostoru a pokud ano tak
 * ho otevře
 * @param parametry - jako  parametr obsahuje jméno prostoru
 * @return zpráva, kterou vypíše hra hráči
 * 
 */
    @Override
	public String provedPrikaz(String... parametry) {
		   if (parametry.length == 0) {
	            // pokud chybí druhé slovo (sousední prostor), tak ....
	            return "Co mám odemknout? Musíš zadat jméno východu";
	        }

	        String smer = parametry[0];

	       
	        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

	        if (sousedniProstor == null) {
	        	
	            return "To se odsud odemknout nedá!";
	        }
	        if(smer.equals("mistnost_s_pokladem")&&!plan.getBatoh().ObsahujeVec("drahokam")){
	        	return "zdá se že na dveřích je symbol trolla, každopádně ty dveře nemám čím odemknout";
	        }
	        if(smer.equals("brána")&&!plan.getBatoh().ObsahujeVec("klic")){
	        	return "tohle je zamčené";
	        }
	        else {
	        	
	        	 sousedniProstor.zamknout(false);
	                 return "dveře do místnosti "+sousedniProstor.getNazev()
	                      +" jsou odemčené";
	             
	          
	        }
		
	}

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}
	

}
