package logika;

import util.ObserverZmeny;
import util.SubjektZmeny;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan implements SubjektZmeny {
    
    private Prostor aktualniProstor;
    private Batoh batoh= new Batoh();
	private Set<ObserverZmeny> mnozinaPozorovatelu = new HashSet<>();

	/**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví nádraží.
     */
    public HerniPlan() {
        zalozPrvkyHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Vytváří věci a postavy, a určuje v jakých prostorech se vyskytují.
     *  Jako výchozí aktuální prostor nastaví nádraží.
     */
    private void zalozPrvkyHry() {
    	
    	Prostor nádraží = new Prostor("nádraží","nádraží. Toto je hlavní Káhirské nádraží, největší v zemi. \n Odsud začíná vaše cesta za vykopávkami,",60.0,170.0);
    	Prostor město = new Prostor("město","Káhira, město kde pracujete na vykopávkách",220.0,170.0);
    	Prostor vykopávky = new Prostor("vykopávky","Egyptské vykopávky z dob faraonů. Počkat, kdo to támhle je?",220.0,250.0);
    	Prostor duny = new Prostor("duny"," tajemné duny. Většina obyvatel se sem ani neodvažuje",400.0,170.0);
    	Prostor jeskyně = new Prostor("jeskyně","jeskyně. Hmm, hodně neobvyklý nález uprostřed pouště",550.0,170.0);
    	Prostor podzemí = new Prostor("podzemí","podzemí. Jsou tu známky liské činnosti, kdy tu asi žili lidé? a kde jsou teď??",670.0,250.0);
    	Prostor brána = new Prostor("brána","brána. Takhle stará brána snad už ani nemůže fungovat",670.0,170.0);
    	Prostor tajemnáMístnost= new Prostor("tajemná_místnost"," tajemná místnost. Zdá se že v rohu někdo je! Nebo to je jen nějaké zvíře?",670.0,335.0);
    	Prostor prokletáMístnost = new Prostor("prokletá_místnost","prokletá místnost.V téhle místnosti nesmím zůstat dlouho, něco ze mě vysává sílu...cítím se tak.......slabý",850.0,335.0);
    	Prostor most = new Prostor("most","Most. Tenhle most je snad nekonečný",550.0,335.0);
    	Prostor místnostSPokladem = new Prostor("místnost_s_pokladem"," místnost s pokladem. To je ono, to je to co jsem hledal!",400.0,335.0);
    	
    	nádraží.setVychod(město);
    	město.setVychod(nádraží);
    	město.setVychod(vykopávky);
    	město.setVychod(duny);
    	vykopávky.setVychod(město);
    	duny.setVychod(jeskyně);
    	duny.setVychod(město);
    	jeskyně.setVychod(duny);
    	jeskyně.setVychod(brána);
    	brána.setVychod(jeskyně);
    	brána.setVychod(podzemí);
    	podzemí.setVychod(brána);
    	podzemí.setVychod(tajemnáMístnost);
    	podzemí.setVychod(prokletáMístnost);
    	podzemí.setVychod(most);
    	tajemnáMístnost.setVychod(podzemí);
    	prokletáMístnost.setVychod(podzemí);
    	most.setVychod(podzemí);
    	most.setVychod(místnostSPokladem);
    	
    	místnostSPokladem.zamknout(true);
    	brána.zamknout(true);
    	
    	aktualniProstor=nádraží;
    	
    	  Vec stul = new Vec("stul",true);
    	  
    	  Vec bylinky = new Vec("bylinky",true);
    	  duny.vlozVec(bylinky);
    	 
         nádraží.vlozVec(stul);
		//nádraží.vlozVec(bylinky);
         
         
         Čaroděj carodej = new Čaroděj("čaroděj", bylinky,new Vec("hulka",true) , "Zdravím tě cizinče. Vím co zde hledáš, přines mi bylinky co rostou v poušti a já ti pomůžu. ", "Tohle ti pomůže proti tomu co tě na cestě čeká.\n Cítím z tebe sílu magie, to je dobře, abys tohlemohl použít, musíš říct nějaké zaklínadlo,\n jsem si jist že na něco přijdeš.", "snažíš se mě obelhat?", "přesně tyhle bylinky jsem hledal, na tady máš něco na oplátku");
         Stařec starec = new Stařec("stařec", null, null, "Poslechni, že ty nejsi odsud? Ahh další ztracená duše hledá poklad Antiků, \n no stejně mě neposlechneš jako všichni ostatní. Nemá cenu ti něco rozmlouvat.\n No dobře, když ti to nemohu rozmluvit, tak ti aspoň trochu poradím, chceš?", "tohle ti snad pomůže, bohužel už si nevzpomínám s čím. \n Získal jsi nějaký klíč", "eh? ", "chci to",this);
         // Troll troll = new Troll("troll", null, null, "bleeeeee ble chro chroooooooo", "uuueeeueueu chroooo ham ham", "eeeeeeeeeeeeeeeee", "dík");
         Drak drak = new Drak("drak", null, null, "Kdo si myslíš že jsi vstoupit do mé jeskyně človíčku, proti mě jsou tvé zbraně bezcenné", "hahahahaha, zde zemřeš", "mě ničím neuplatíš", "hmmm, tohle není špatné");
         
         město.vlozPostavu(starec);
       //  prokletáMístnost.vlozPostavu(troll);
         místnostSPokladem.vlozPostavu(drak);
         vykopávky.vlozPostavu(carodej);
         
        
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {

    	aktualniProstor = prostor;
    	upozorniPozorovatele();
    }
    /**
     * metoda vrací batoh hráče
     * @return Batoh který hráč používá
     */
    public Batoh getBatoh(){
    	return batoh;
    }
	/**
	 * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
	 * která implementuje rozhraní ObserverZmeny.
	 *
	 * @param pozorovatel registrovaný objekt
	 */
	@Override
	public void zaregistrujPozorovatele(ObserverZmeny pozorovatel) {
		mnozinaPozorovatelu.add(pozorovatel);
	}
	/**
	 * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
	 * která implementuje rozhraní ObserverZmeny.
	 *
	 * @param pozorovatel objekt, který již nechce být informován o změnách
	 */
	@Override
	public void odregistrujPozorovatele(ObserverZmeny pozorovatel) {
    	mnozinaPozorovatelu.remove(pozorovatel);

	}
	/**
	 * Metoda, která se volá vždy, když dojde ke změně této instance.
	 * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
	 */
	@Override
	public void upozorniPozorovatele() {
		for (ObserverZmeny observerZmeny : mnozinaPozorovatelu) {
			observerZmeny.aktualizuj();
		}


	}
}
