package logika;
/**
 * třídá PrikazDej zajištuje výměnu mezi postavou a hráčem
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazDej implements IPrikaz {
	 private static final String NAZEV = "dej";
	    private HerniPlan plan;
	    
	    public PrikazDej(HerniPlan plan){
	    	this.plan=plan;
	    }

	    /**
	     * předá předmět z batohu postavě a ta předá předmět do batohu
	     * @param parametry - jako  parametr obsahuje jméno postavy,
         *  a předmět který má předat
         *@return zpráva, kterou vypíše hra hráči
	     */
	@Override
	public String provedPrikaz(String... parametry) {
		// TODO Auto-generated method stub

        Batoh batoh = plan.getBatoh();
        Prostor aktualniProstor = plan.getAktualniProstor();
        if (parametry.length == 0) {
            return "Co mám jako dát? Musíš mi to říct!";
        }
        else if(parametry.length==1) {
            return "Komu to mám jako dát?";
        }
        else if(parametry.length == 2 && batoh.ObsahujeVec(parametry[0])) {
            if(aktualniProstor.obsahujePostavu(parametry[1])) {
                IPostava postava = aktualniProstor.vratPostavu(parametry[1]);
                Vec vecKterouChce = postava.getVecKterouChce();
                Vec vecKterouVrati = postava.getVecKterouVrati();
                if(vecKterouChce!=null && postava.getJizMluvil()) {
                    if (vecKterouChce.getJmeno().equals(parametry[0])) {
                    	if(!batoh.jePlny()){
                       
                    	postava.setProbehlaVymena(true);
                    	postava.setVecKterouVrati(null);
                    	postava.setVecKterouChce(null);
                    	batoh.vlozDoBatohu(vecKterouVrati);
                    	batoh.odeberVecZBatohu(vecKterouChce.getJmeno());
                    	return postava.getRekneKdyzVecChce();
                    	}else{
                    		
                    	return "pockej máš plnej batoh";
                    	}
                    	}
                    else {
                        return postava.getRekneKdyzVecNechce();
                    }
                }
                else if(postava.getJizMluvil()) {
                    return postava.getRekneKdyzVecNechce();
                }
                else {
                    return  postava.getJmeno() + " na tebe nechápavě kouká";
                }
            }
            else {
                return "nikdo takový tu není";
            }

        }
        else if(parametry.length==2) {
            return "Tohle u sebe nemám.";
        }
        else {
            return "moc parametrů";
        }
	}

	/**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
