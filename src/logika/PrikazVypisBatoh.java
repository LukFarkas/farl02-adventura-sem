package logika;
/**
 * třída implementuje pro hru vypsání batohu
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazVypisBatoh implements IPrikaz{
	
	 private static final String NAZEV = "vypisBatoh";
	 private HerniPlan plan;
	 
	  public PrikazVypisBatoh(HerniPlan plan) {
		 this.plan=plan;
	}

	  /**
	   * vypíše věci které jsou ted v batohu hráče
	   * @param žádné parametry
	   *@return zpráva, kterou vypíše hra hráči
	   */
	@Override
	public String provedPrikaz(String... parametry) {
		if(parametry.length>0){
			return "za tenhle prikaz nic nepis";
		}
		return plan.getBatoh().veciVBatohu();
	}

	/**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
