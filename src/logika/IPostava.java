package logika;

/**
 * rozhraní IPostava slouží k propojení všech postav ve hře
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public interface IPostava {
	
public String mluv();

public String getJmeno();

public Vec getVecKterouChce();

public void setVecKterouChce(Vec vecKterouChce);

public Vec getVecKterouVrati();

public void setVecKterouVrati(Vec vecKterouVrati);

public Boolean getJizMluvil();

public void setProbehlaVymena(Boolean probehlaVymena);

public String getReknePoVymene();

public String getRekneKdyzVecNechce();

public String getRekneKdyzVecChce();
}
