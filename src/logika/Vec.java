package logika;

/**
 * třída Vec představuje věci které hráč sbírá 
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class Vec {
	 private String jmeno;
	   private boolean prenositelna;

	   private boolean prozkoumana = false;
	   //private Map<String,Vec> seznamVeci;

	   /**
	    * konstruktor třídy vytváří jméno věci a jestli je přenositelná
	    */
	   public Vec (String jmeno, boolean prenositelna) {
	    this.jmeno = jmeno;
	    this.prenositelna = prenositelna;
	   // seznamVeci = new HashMap<String,Vec>();
	   }
	   /**
	    * jméno věci
	    * @return String se jménem
	    */
	   public String getJmeno () {
	    return jmeno;
	   }
	   /**
	    * zjistí jestli je věc přenositelná
	    * @return true pokud je věc přenositelná, false pokud ne
	    */
	   public boolean jePrenositelna() {
	    return prenositelna;
	   }

	   /**
	    * zjistí jestli je věc prozkoumána
	    * @return true pokud je věc prozkoumána, false pokud ne
	    */
	    public boolean jeProzkoumana() {
	        return prozkoumana;
	    }

	    /**
	     * nastaví jestli je věc prozkoumána
	     * @param true pokud je věc přenositelná, false pokud ne
	     */
	    public void prozkoumano (boolean prozkoumana) {
	        this.prozkoumana = prozkoumana;
	    }

	    /**
	     * 
	     * @param String jmeno věci
	     */
	    public String toString() {
	        return jmeno;
	    }

}
