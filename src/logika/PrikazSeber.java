package logika;
/**
 * třída PrikazSeber implementuje pro hru sbírání věcí
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazSeber implements IPrikaz{
	   private static final String NAZEV = "seber";
	    private HerniPlan plan;
	    
	    public PrikazSeber(HerniPlan plan){
	    	this.plan = plan;
	    }

	    /**
	     * sebere věc která je v aktuálním prostoru
	     *@param parametry - Věc kterou chce hráč sebrat
	     *@return zpráva, kterou vypíše hra hráči
	     * 
	     */
	@Override
	public String provedPrikaz(String... parametry) {
		if(parametry.length == 0){
			return "co mám sebrat ? Musíš mi to říct.";
		}
		 else if(parametry.length == 1 && this.plan.getAktualniProstor().getVecZProstoru(parametry[0]).jeProzkoumana()) {
			 
	            Vec vec = this.plan.getAktualniProstor().getVecZProstoru(parametry[0]);
	            if(vec.jePrenositelna()) {
	            	if(!plan.getBatoh().jePlny()){
	            	plan.getBatoh().vlozDoBatohu(vec);
	            	plan.getAktualniProstor().odeberVec(parametry[0]);
	                return  vec.getJmeno() + " je nyní v batohu";
	            }else {
	            	return "počkej máš plnej batoh";
	            }
	            }
		 }
		return "tohle nemůžu najít";
	}

	 /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
