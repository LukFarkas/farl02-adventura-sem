package logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {
    private Hra hra1;

    //== Datové atributy (statické i instancí)======================================

    Troll troll =new Troll("troll", null, null, "bleeeeee ble chro chroooooooo", "uuueeeueueu chroooo ham ham", "eeeeeeeeeeeeeeeee", "dík");
    Vec drahokam =new Vec("drahokam", true);
    Vec hulka = new Vec("hůlka",true);
    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra1 = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     *  Testuje i jaké osoby jsou v místnosti a jaké věci jsou v batohu hráče.
     */
    @Test
    public void testÚspěšnýPrůbehHry() {
    	assertEquals("nádraží", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi město");
    	assertEquals(false, hra1.konecHry());
    	//zjistí jestli tu je stařec
    	assertEquals(true, hra1.getHerniPlan().getAktualniProstor().obsahujePostavu("stařec"));
    	//zjistí jestli máme klíč v batohu
    	assertEquals(false, hra1.getHerniPlan().getBatoh().ObsahujeVec("klíč"));
    	//získá klíč
    	hra1.zpracujPrikaz("mluv stařec");
    	assertEquals(false, hra1.konecHry());
    	hra1.zpracujPrikaz("mluv stařec");
    	assertEquals(false, hra1.konecHry());
    	//potvrdí že máme klíč v batohu
    	assertEquals(true, hra1.getHerniPlan().getBatoh().ObsahujeVec("klíč"));
    	assertEquals("město", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi duny");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("duny", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("prozkoumej");
    	assertEquals(false, hra1.konecHry());
    	hra1.zpracujPrikaz("seber bylinky");
    	assertEquals(false, hra1.konecHry());
    	//sebral bylinky
    	assertEquals(true, hra1.getHerniPlan().getBatoh().ObsahujeVec("bylinky"));
    	hra1.zpracujPrikaz("jdi město");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("město", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi vykopávky");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("vykopávky", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	//zjistí jestli tu je čaroděj
    	assertEquals(true, hra1.getHerniPlan().getAktualniProstor().obsahujePostavu("čaroděj"));
    	hra1.zpracujPrikaz("mluv čaroděj");
    	assertEquals(false, hra1.konecHry());
    	hra1.zpracujPrikaz("dej bylinky čaroděj");
    	assertEquals(false, hra1.konecHry());
    	//zkontoluje že jsme dostali hůlku
    	assertEquals(false, hra1.getHerniPlan().getBatoh().ObsahujeVec("bylinky"));
    	assertEquals(true, hra1.getHerniPlan().getBatoh().ObsahujeVec("hůlka"));
    	assertEquals("vykopávky", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi město");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("město", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi duny");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("duny", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi jeskyně");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("jeskyně", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	//brána je zamčená
    	hra1.zpracujPrikaz("jdi brána");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("jeskyně", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("odemkni brána");
    	assertEquals(false, hra1.konecHry());
    	hra1.zpracujPrikaz("jdi brána");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("brána", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi podzemí");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("podzemí", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.zpracujPrikaz("jdi most");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("most", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	hra1.getHerniPlan().getBatoh().vlozDoBatohu(drahokam);
    	hra1.zpracujPrikaz("odemkni místnost_s_pokladem");
    	assertEquals(false, hra1.konecHry());
    	hra1.zpracujPrikaz("jdi místnost_s_pokladem");
    	assertEquals(false, hra1.konecHry());
    	assertEquals("místnost_s_pokladem", hra1.getHerniPlan().getAktualniProstor().getNazev());
    	assertEquals(true, hra1.getHerniPlan().getAktualniProstor().obsahujePostavu("drak"));
    	hra1.zpracujPrikaz("čaruj drak beulls");
    	assertEquals(true, hra1.konecHry());	
    }
    /**
     * pokud postava narazí na trola a prohraje souboj, skončí hra
     */
    @Test
    public void testNeůspěšnýPrůbehHry() {
    	hra1.getHerniPlan().getAktualniProstor().vlozPostavu(troll);
    	hra1.getHerniPlan().getBatoh().vlozDoBatohu(hulka);
    	hra1.zpracujPrikaz("čaruj troll ttttttttt");
    	assertEquals(true, hra1.konecHry());
    	
    	
    }
}
