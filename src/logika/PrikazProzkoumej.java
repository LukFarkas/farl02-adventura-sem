package logika;

import java.util.Map;

/**
 * třída PrikazProzkoumej implementuje pro hru prozkoumávání prostorů
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazProzkoumej implements IPrikaz {
	
	 private static final String NAZEV = "prozkoumej";
	    private HerniPlan plan;
	    
	    public PrikazProzkoumej(HerniPlan plan){
	    	this.plan=plan;
	    }

	    /**
	     * zjistí jestli se nějaká věc nachází v aktuálním prostoru
	     *@param žádné parametry
	     *@return zpráva, kterou vypíše hra hráči
	     */
	@Override
	public String provedPrikaz(String... parametry) {
        String vracenyText = "Tohle jsem tu našel: ";
        if(plan.getAktualniProstor().getSeznamVeci().size()>0) {
            for (Map.Entry<String, Vec> vec : plan.getAktualniProstor().getSeznamVeci().entrySet()) {
            	vec.getValue().prozkoumano(true);
            	if(vec.getValue().jeProzkoumana()){
                vracenyText += vec.getValue().getJmeno() + ",";
            	}
            }
            return vracenyText;
        }
        else {
            return vracenyText = "Nic tu není";
        }
		
	}

	 /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
