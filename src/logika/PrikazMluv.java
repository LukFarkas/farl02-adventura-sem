package logika;

/**
 * třída přikazMluv zajišťuje komunikaci mezi postavou a hráčem
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazMluv implements IPrikaz {
	
	   private static final String NAZEV = "mluv";
	    private HerniPlan plan;
	    
	    public PrikazMluv(HerniPlan plan){
	    	this.plan = plan;
	    }

	    /**
	     * začne mluvit s postavou která je v aktualním prostoru
	     * @param parametry - jako  parametr obsahuje jméno postavy,
	     */
	@Override
	public String provedPrikaz(String... parametry) {
		 if (parametry.length == 0) {
	            
	            return "Na koho mám mluvit? musíš mi to říct...";
	        }
	        else if(parametry.length == 1 && plan.getAktualniProstor().obsahujePostavu(parametry[0])) {
	            
	            return plan.getAktualniProstor().vratPostavu(parametry[0]).mluv();

	        }

	        return "Tenhle takový tu neni!";
	}

	/**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

}
