package logika;

import java.util.Random;
/**
 * třída PrikazCaruj představuje souboj s postavami a hráčem ve hře
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class PrikazCaruj implements IPrikaz{
	
	 private static final String NAZEV = "čaruj";
	    private Hra hra;
	    private HerniPlan plan;
	    public PrikazCaruj(Hra hra, HerniPlan plan){
	    	this.plan=plan;
	    	this.hra=hra;
	    }

	    /**
	     * začne souboj s postavou ve hře, hráč musí zadat jmeno postavy která se nachází
	     * v aktualním prostoru a hráčem vytvořené "kouzlo" podle kterého se rozhodne výsledek boje
	     * hráč potřebuje hůlku aby mohl kouzlit
	     *
	     * *@param parametry - jako  parametr obsahuje jméno postavy,
         *  a kouzlo které má použít
         *@return zpráva, kterou vypíše hra hráči
	     */
	@Override
	public String provedPrikaz(String... parametry) {
		// TODO Auto-generated method stub
		if(!plan.getBatoh().ObsahujeVec("hulka")){return "nemám s čím čarovat";}
		
		
		 if (parametry.length == 0) {
	          
	            return "Koho mám jako začarovat? Musíš mi to říct!";
	        }
	        else if(parametry.length==1) {
	            return "musíš říct nějaké zaklínadlo";
	        }
		 if(parametry[1].length()<5){return "takhle krátké kouzlo nebude fungovat";}
		 
		 
		 //caruj s troll
		 if(parametry[0].equals("troll")&&plan.getAktualniProstor().obsahujePostavu("troll")){
			 if(parametry[1].contains("t")||parametry[1].contains("r")||parametry[1].contains("o")||parametry[1].contains("l")){
				 hra.setKonecHry(true);
				 return spatnaOdpoved("troll")+ "\n budeš to muse zkusit znovu \n ";
				 
			 }else{
				 plan.getAktualniProstor().odstranPostavu("troll");
				 plan.getAktualniProstor().vlozVec(new Vec("drahokam",true));
				 return spravnaOdpoved("troll") + "\n zdá se že z trola něco vypalo na zem";
				
				 
			 }	 
		 }
		 
		 //caruj s drak
		 else if(parametry[0].equals("drak")&&plan.getAktualniProstor().obsahujePostavu("drak")){
			 if(parametry[1].contains("d")||parametry[1].contains("r")||parametry[1].contains("a")||parametry[1].contains("k")){
				 hra.setKonecHry(true);
				 return spatnaOdpoved("drak")+ "\n budeš to muse zkusit znovu \n ";
				
			 }else{
				
				 hra.setKonecHry(true);
				 return spravnaOdpoved("drak")+ "\n gratulujeme, díky tomu že jsi dokázal porazi draka jsi pravděpodobně zachránil město \n a k tomu jsi získal poklad prastaré civilizace. Druhá část hry bude v příštím semstru, tak se těšte :)";
				 //doplnit co odstane
			 }	 
		 }
		 
		 //caruj ostatní
		 return "s tímhle nemůžu bojovat";
		 
		
	}

	/**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
	@Override
	public String getNazev() {
		// TODO Auto-generated method stub
		return NAZEV;
	}

	/**
	 * náhodně vybere špatnou odpověd na souboj
	 * @param jmeno postavy
	 * @return String s odpovědí
	 */
	public String spatnaOdpoved(String jmeno){
		Random randomGenerator = new Random();
		int randomCislo = randomGenerator.nextInt(3);
		switch(randomCislo) {
		  case 1:
		    return jmeno + "se vyhnul tvému kouzlu a zabil tě.";
		    
		  case 2:
		    return "tohle kouzlo bylo na " + jmeno+ "a moc slabé. Poté co se " + jmeno+ " vzpamatoval, ti ukousnul hlavu.";
		    
		  case 0:
			 return  jmeno+ " tě stihl rozsápat dřív, než jsi stačil doříct kouzlo";
		  default:
		    return "tak tohle se stát nemělo, ale stejně tě to zabilo";
		}
		
	}
	
	/**
	 * náhodně vybere správnou odpověd na souboj
	 * @param jmeno postavy
	 * @return String s odpovědí
	 */
	public String spravnaOdpoved(String jmeno){
		Random randomGenerator = new Random();
		int randomCislo = randomGenerator.nextInt(3);
		switch(randomCislo) {
		  case 1:
		    return "dokázal jsi vytvořit takové kouzlo, že "+ jmeno+ " neměl žadnou šanci. Padl mrtev.";
		    
		  case 2:
		    return jmeno+ " se proměnil v kámen, zdá se že tohle bylo správné kouzlo.";
		    
		  case 0:
			 return  jmeno+ " jen zavřískal a padl k zemi, tohle bylo opravdu silné kouzlo.";
		  default:
		    return "tak tohle se stát nemělo, ale stejně jsi vyhrál.";
		}
		
	}
}