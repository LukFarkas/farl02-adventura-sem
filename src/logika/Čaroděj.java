package logika;
/**
 * postava čaroděj promlouvá s hráčem, a dá mu hůlku potřenou pro PříkazČaruj 
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class Čaroděj implements IPostava{
	
    private String jmeno;
    private Vec vecKterouChce;
    private Vec vecKterouVrati;
    private String reknePredVymenou;
    private String reknePoVymene;
    private String rekneKdyzVecNechce;
    private String rekneKdyzVecChce;
    private Boolean jizMluvil = false;
    private Boolean probehlaVymena = false;
  
    /**
     * konstruktor třídy který nastaví vlastnosti postavy
     */
	public Čaroděj(String jmeno, Vec vecKterouChce, Vec vecKterouVrati, String reknePredVymenou,
			String reknePoVymene, String rekneKdyzVecNechce, String rekneKdyzVecChce) {
		this.jmeno = jmeno;
		this.vecKterouChce = vecKterouChce;
		this.vecKterouVrati = vecKterouVrati;
		this.reknePredVymenou = reknePredVymenou;
		this.reknePoVymene = reknePoVymene;
		this.rekneKdyzVecNechce = rekneKdyzVecNechce;
		this.rekneKdyzVecChce = rekneKdyzVecChce;
	}
	
	

	/**
	 * vrátí jméno
	 * @return String jméno postavy
	 */
	public String getJmeno() {
		return jmeno;
	}

	/**
	 * nastaví jméno postavy
	 * @param string jméno postavy
	 */
	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}

	/**
	 * vrátí věc kterou postava chce
	 * @return vec kterou chce
	 */
	public Vec getVecKterouChce() {
		return vecKterouChce;
	}

	/**
	 * nastaví věc kterou postava chce
	 * @param Vec kterou chce
	 */
	public void setVecKterouChce(Vec vecKterouChce) {
		this.vecKterouChce = vecKterouChce;
	}

	/**
	 * vrátí věc kterou postava vymění
	 * @return vec Kterou Vrátí
	 */
	public Vec getVecKterouVrati() {
		return vecKterouVrati;
	}

	/**
	 * nastaví věc kterou postava vymění 
	 * @param vec kterou vrátí
	 */
	public void setVecKterouVrati(Vec vecKterouVrati) {
		this.vecKterouVrati = vecKterouVrati;
	}

	/**
	 * co postava řekne před výměnou
	 * @return String co řekne
	 */
	public String getReknePredVymenou() {
		return reknePredVymenou;
	}

	/**
	 * nastaví co řekne před výměnou
	 * @param String co má říct
	 */
	public void setReknePredVymenou(String reknePredVymenou) {
		this.reknePredVymenou = reknePredVymenou;
	}

	/**
	 * co postava řekne po výměně
	 * @return String co řekne
	 */
	public String getReknePoVymene() {
		return reknePoVymene;
	}

	/**
	 * nastaví co postava řekne po výměně
	 * @param String co řekne
	 */
	public void setReknePoVymene(String reknePoVymene) {
		this.reknePoVymene = reknePoVymene;
	}

	/**
	 * co postava rekne když věc nechce
	 * @return String co řekne
	 */
	public String getRekneKdyzVecNechce() {
		return rekneKdyzVecNechce;
	}

	/**
	 * nastaví co rekne když věc nechce
	 * @param String co má říct
	 */
	public void setRekneKdyzVecNechce(String rekneKdyzVecNechce) {
		this.rekneKdyzVecNechce = rekneKdyzVecNechce;
	}

	/**
	 *co postava rekne když věc chce
	 * @return String co řekne
	 */
	public String getRekneKdyzVecChce() {
		return rekneKdyzVecChce;
	}

	/**
	 * * nastaví co rekne když věc chce
	 * @param String co má říct
	 */
	public void setRekneKdyzVecChce(String rekneKdyzVecChce) {
		this.rekneKdyzVecChce = rekneKdyzVecChce;
	}

	/**
	 * zjistí jestli mluvil
	 * @return true pokud mluvil, false pokud ne
	 */
	public Boolean getJizMluvil() {
		return jizMluvil;
	}

	/**
	 * nastaví jestli postava mluvila 
	 * @param true pokud mluvil, false pokud ne
	 */
	public void setJizMluvil(Boolean jizMluvil) {
		this.jizMluvil = jizMluvil;
	}

	/**
	 * zjistí jestli proběhla výměna věcí
	 * @return true pokud mluvil, false pokud ne
	 */
	public Boolean getProbehlaVymena() {
		return probehlaVymena;
	}

	/**
	 * nastaví jestli proběhla výměna věcí
	 * @param true pokud mluvil, false pokud ne
	 */
	public void setProbehlaVymena(Boolean probehlaVymena) {
		this.probehlaVymena = probehlaVymena;
	}
	
	/**
	 * metoda určuje co postava řekne
	 * @return reknePoVymene pokud mluvil, reknePredVymenou pokud ne
	 */
	public String mluv(){
	        jizMluvil = true;
	        if(!probehlaVymena) {
	            return jmeno + ": " + reknePredVymenou;
	        }
	        else{
	            return jmeno+ ": " + reknePoVymene;
	        }	    
	}

}
