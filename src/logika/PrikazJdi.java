package logika;

import java.util.Random;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        else {
        	 if (sousedniProstor.jeZamcena()) {
                 return "dveře do místnosti "+sousedniProstor.getNazev()
                      +" jsou zamčené";
             }
        	 
        	 if(plan.getAktualniProstor().getNazev().equals("prokletá_místnost")||plan.getAktualniProstor().getNazev().equals("tajemná_místnost")){
        		 if(plan.getAktualniProstor().obsahujePostavu("troll")){
        			 plan.getAktualniProstor().odstranPostavu("troll");
        		 }
        	 }
        	 //
            plan.setAktualniProstor(sousedniProstor);
            
        	if(smer.equals("prokletá_místnost")||smer.equals("tajemná_místnost")){
        		// náhodně se spawnující troll
        		Random randomGenerator = new Random();
        		int randomCislo = randomGenerator.nextInt(2);        		
        		if(randomCislo==0){
            plan.getAktualniProstor().vlozPostavu( new Troll("troll", null, null, "bleeeeee ble chro chroooooooo", "uuueeeueueu chroooo ham ham", "eeeeeeeeeeeeeeeee", "dík"));
        		}
        	}
        	
            return sousedniProstor.dlouhyPopis();
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
