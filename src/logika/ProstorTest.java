package logika;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    Jarmila Pavlíčková
 * @version   pro skolní rok 2016/2017
 */
public class ProstorTest
{
    //== Datové atributy (statické i instancí)======================================
	 Troll troll =new Troll("troll", null, null, "bleeeeee ble chro chroooooooo", "uuueeeueueu chroooo ham ham", "eeeeeeeeeeeeeeeee", "dík");
	   
    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /**
     * Testuje, zda jsou správně nastaveny průchody mezi prostory hry. Prostory
     * nemusí odpovídat vlastní hře, 
     */
    @Test
    public  void testLzeProjit() {		
        Prostor prostor1 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě",200.0,300.0);
        Prostor prostor2 = new Prostor("bufet", "bufet, kam si můžete zajít na svačinku",200.0,300.0);
        prostor1.setVychod(prostor2);
        prostor2.setVychod(prostor1);
        assertEquals(prostor2, prostor1.vratSousedniProstor("bufet"));
        assertEquals(null, prostor2.vratSousedniProstor("pokoj"));
    }
    
    
    /**
     * Testuje veci v prostoru
     */
    @Test
    public  void testVeci() {		
        Prostor prostor1 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě",200.0,300.0);
        Prostor prostor2 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě",200.0,300.0);
        prostor1.vlozVec(new Vec("vec1",true));
        prostor1.vlozVec(new Vec("vec2",true));
        assertEquals(true, prostor1.obsahujeVec("vec1"));
        assertEquals(false, prostor2.obsahujeVec("vec2"));
    }
    
    /**
     * Testuje osoby v prostoru
     */
    @Test
    public  void testOsob() {		
        Prostor prostor1 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě",200.0,300.0);
        Prostor prostor2 = new Prostor("hala", "vstupní hala budovy VŠE na Jižním městě",200.0,300.0);
        prostor1.vlozPostavu(troll);
        assertEquals(true, prostor1.obsahujePostavu("troll"));
        assertEquals(false, prostor2.obsahujePostavu("troll"));
    }
    


}
