package logika;

import javafx.print.JobSettings;
import util.ObserverZmeny;
import util.SubjektZmeny;

import java.util.*;

/**
 * Třída batoh eviduje věci které u sebe hráč uchováná
 * @author Lukáš Farkas
 * @version    pro školní rok 2019/2020 
 *
 */
public class Batoh implements SubjektZmeny {
	private Map<String, Vec>seznamVeci = new HashMap<>();
	private Set<ObserverZmeny> mnozinaPozorovatelu = new HashSet<>();
	
	/**
	 * Pokud batoh není plný vloží věc do batohu 
	 * @param vec kterou chceme vložit
	 */
	public void vlozDoBatohu(Vec vec){

		if(!jePlny()){
			seznamVeci.put(vec.getJmeno(),vec);
			upozorniPozorovatele();

		}

	}
	
	/**
	 * Odebere věc z batohu
	 * @param jmeno věci kterou chceme odebrat
	 * @return vec kterou jsme právě odebrali
	 */
	public Vec odeberVecZBatohu(String jmeno){
		Vec vec = null;
		if(seznamVeci.containsKey(jmeno)){
			vec = seznamVeci.get(jmeno);
			seznamVeci.remove(jmeno);
			upozorniPozorovatele();
		}
		return vec;
	}
	
	/**
	 * projde seznam vecí so má hráč u sebe v batohu
	 * @return string obsahující věci a informační text 
	 */
	public String veciVBatohu(){
		String veci="v Batohu máš:";
		if(seznamVeci.isEmpty()){
			return "nic nemáš";
		}
		for (Map.Entry<String, Vec> vec : seznamVeci.entrySet()) {
            veci += vec.getKey() + ", ";
        }
		return veci;

	}

	public Set<String> getNazvyVeci() {
		return Collections.unmodifiableSet(seznamVeci.keySet());
	}

	/**
	 * zkontroluje jestli je věc v batohu
	 * @param jmeno věci kterou chceme zkontrolovat
	 * @return vrátí true pokud věc je v batohu a false pokud ne
	 */
	public boolean ObsahujeVec(String jmeno){
		return seznamVeci.containsKey(jmeno);
	}
	
	/**
	 * zkontoluje jestli je batoh plný
	 * @return vrátí true pokud je batoh plný a false pokud ne
	 */
	   public Boolean jePlny() {
	        if(seznamVeci.size()>3) {
	            return true;
	        }
	        return false;
	    }
	/**
	 * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
	 * která implementuje rozhraní ObserverZmeny.
	 *
	 * @param pozorovatel registrovaný objekt
	 */
	@Override
	public void zaregistrujPozorovatele(ObserverZmeny pozorovatel) {
		mnozinaPozorovatelu.add(pozorovatel);
	}

	/**
	 * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
	 * která implementuje rozhraní ObserverZmeny.
	 *
	 * @param pozorovatel objekt, který již nechce být informován o změnách
	 */
	@Override
	public void odregistrujPozorovatele(ObserverZmeny pozorovatel) {
	   	mnozinaPozorovatelu.remove(pozorovatel);
	}

	/**
	 * Metoda, která se volá vždy, když dojde ke změně této instance.
	 * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
	 */
	@Override
	public void upozorniPozorovatele() {
		for (ObserverZmeny observerZmeny : mnozinaPozorovatelu) {
			observerZmeny.aktualizuj();
		}


	}
}
