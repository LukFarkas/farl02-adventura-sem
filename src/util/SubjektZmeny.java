package util;

/**
 * rozhraní SubjektZmeny predstavuje subjekt, je součástí návrhového vzoru observer
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public interface SubjektZmeny {

    /**
     * Metoda slouží k zaregistrování pozorovatele, musí to být instance třídy,
     * která implementuje rozhraní ObserverZmeny.
     *
     * @param pozorovatel registrovaný objekt
     */
   void zaregistrujPozorovatele(ObserverZmeny pozorovatel);

    /**
     * Metoda slouží k zrušení registrace pozorovatele, musí to být instance třídy,
     * která implementuje rozhraní ObserverZmeny.
     *
     * @param pozorovatel objekt, který již nechce být informován o změnách
     */
    void odregistrujPozorovatele(ObserverZmeny pozorovatel);

    /**
     * Metoda, která se volá vždy, když dojde ke změně této instance.
     * Upozorní všechny pozorovatele, že došlo ke změně tak, že zavolá jejich metodu aktualizuj.
     */
    void upozorniPozorovatele();
}
