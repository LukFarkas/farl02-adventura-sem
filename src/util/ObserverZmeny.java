package util;

/**
 * rozhraní ObserverZmeny predstavuje objekt, je součástí návrhového vzoru observer
 * @author Lukáš Farkas
 * @version    pro školní rok 2020/2021
 *
 */
public interface ObserverZmeny {

    /**
     *metoda ve které probehne aktualizace pozorovatele
     *
     */
    public void aktualizuj();

}
